# DOCKER COMMANDS
docker images - show all images
docker ps - show all running containers
docker ps -a - show all containers
docker ps -a -q - show only all containers id CONTAINER ID
docker rm - delete container(must be transferred CONTAINER ID or NAME)
docker rm $(docker ps -qa) delete all containers
docker build -t hwa . - create project with name hwa in current folder
docker run --rm --name web -p 8080:8080 -e TZ=Europe/Moscow web-hwa - create container with name web and port 8080, timizone Europe/Moscow from image web-hwa that be  removed after stop
docker run --rm --name web -p 8080:8080 -e TZ=Europe/Moscow -v /home/leonid/Документы/TOOLS/DOCKER/app1/resources:/var/www/app/resources web-hwa - create container with name web and port 8080, timizone Europe/Moscow, absoulute path till folder resource from image web-hwa that be  removed after stop
docker run --name HWA hwa - execute image hwa create container with name HWA
docker run --name HWA -d --rm hwa - execute image hwa create container with name HWA, run background, remove container after stopping
docker run --rm --name web -p 8080:8080 -v web:/var/www/app/resources web-hwa - create container with name web, port 8080, absolute path, from image web-hwa
docker stop HWA - stop container with name HWA or transferred CONTAINER ID
docker volume ls - show all docker volumes
docker volume create web - create docker volume with name web

# DOCKER COMPOSE
docker-compose build - create image
docker-compose up -d - background start container
docker-compose exec php bash - enter container
# COMPOSER
composer dump-autoload
# PHPUNIT
./vendor/bin/phpunit tests